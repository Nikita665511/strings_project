const string3 =  function(str = ""){
    let ans = '';

    let date_reg_expr = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;

    if (!str instanceof String) {
        return [];
    }

    let names = ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'];

    //check for date expression
    let isDate = date_reg_expr.test(str);

    if (isDate) {
        let date = str.trim().split('/');
        day = parseInt(date[0]);
        month = parseInt(date[1]);

        if (month > 0 && month <= 12 && day > 0 && day <= 31) {
            return names[month-1];
        }
    }
    return "";
}

module.exports = string3;
      
    
    



    



