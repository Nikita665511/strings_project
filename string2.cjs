const string2 = function(str = "") {
    // regular expression for ipv4
    let reg_expr = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;
    
    if (!str instanceof String) {
        return [];
    }

    let isIpv4 = reg_expr.test(str);

    let num_arr = []
    if (isIpv4) {
        num_arr = str.trim().split('.').map(Number);
    }
    return num_arr;
}

module.exports = string2;
    
