
function string1(str){
    if (typeof str !== "string")
        return  0;
    str=str.replace(',','');
    str=str.replace('$','');


    if (isNaN(str)) {
        return 'Not a Number!';
    }
    else{
        return str;
    }
}

module.exports = string1;