const string5 = function (wordsArr = []) {
    let words = [];
    if (!wordsArr instanceof Array) {
        return "";
    }
    for (let i=0; i<wordsArr.length; i++) {
        if(typeof wordsArr[i] == 'string') {
            words.push(wordsArr[i].trim());
        }
    }
    return words.join(' ');
}

module.exports = string5;

