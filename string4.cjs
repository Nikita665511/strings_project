const string4 = function(obj = {}) {
    if(!obj instanceof Object)
    {
        return NaN;
    }

    let full_name = "";
    if (obj.first_name && typeof obj.first_name == 'string') {
        full_name = obj.first_name.trim().toUpperCase();
    }

    if (obj.middle_name && typeof obj.middle_name == 'string') {
        full_name = full_name.concat(' ', obj.middle_name.trim().toUpperCase());
    }

    if (obj.last_name && typeof obj.last_name == 'string') {
        full_name = full_name.concat(' ', obj.last_name.trim().toUpperCase());
    }

    return full_name;
};

module.exports = string4;
